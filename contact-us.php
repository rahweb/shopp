<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/menu.php");?>
    <div class="contact-us">
        <br>
        <div class="container">
            <h1>تماس باما</h1>
            <hr>
            <div class="row">
                <div class="col-md-3">
                    <div class="contact-info">
                        <img src="https://image.ibb.co/kUASdV/contact-image.png" alt="image"/>
                        <h2>تماس باما</h2>
                        <h4>دوست داریم بیشتر از شما بشنویم !</h4>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="contact-form">
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="fname">نام:</label>
                          <div class="col-sm-10">          
                            <input type="text" class="form-control" id="fname" placeholder="" name="fname">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="lname">نام خانوادگی:</label>
                          <div class="col-sm-10">          
                            <input type="text" class="form-control" id="lname" placeholder="" name="lname">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="email">ایمیل:</label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="" name="email">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-sm-2" for="comment">پیام:</label>
                          <div class="col-sm-10">
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                          </div>
                        </div>
                        <div class="form-group">        
                          <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default">ارسال</button>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="container open-iconic">
            <div class="box">
                <div class="icon"><span class="flaticon flaticon-home"></span></div>
                <div class='details'><h3>خیابان-کوچه-پلاک</h3></div>
            </div>
            
            <div class="box">
                <div class="icon"><span class="flaticon flaticon-telephone-auricular-with-cable"></span></div>
                <div class='details ltr'><h3>021-22334455 _ 0912-3322555</h3></div>
            </div>
            
            <div class="box">
                <div class="icon"><span class="flaticon flaticon-gmail-logo"></span></div>
                <div class='details'><h3>info@gmail.com</h3></div>
            </div>
        </div>
        <br>
        <br>
        <div id="contact" class="map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d492648.81440094014!2d-73.740421967777!3d40.897138203570115!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x89c2f4f214dc56ef%3A0x505b8ddc076c319d!2s1460+Bronx+River+Ave%2C+Bronx%2C+NY+10472%2C+Birle%C5%9Fik+Devletler!3m2!1d40.8355564!2d-73.87640499999999!5e0!3m2!1str!2str!4v1484062277815" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen>
                </iframe>
            </div>
    </div>
    <?php include("blocks/footer.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>