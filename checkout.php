<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
  <body>

    <?php include("blocks/menu.php");?>
    <div class="checkout">
        <br>
        <div class="container">
            <div class="row loc">
                <div class="col-md-6">
                    <ul>
                        <li class="home">
                            <a href=""><span class="flaticon flaticon-home" style="color:#fe9936"></span>فروشگاه اینترنتی</a>
                        </li>
                        <li>
                            <a href="">سبدخرید</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6"></div>
            </div>
            <br>
            <div class="row checkout-box">
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                    <div class="well text">
                        <h5>خلاصه سفارش</h5>
                        <br>
                        <div class="row well-l ">
                            <div class="col-md-6 co-lg-6 col-sm-6 col-xs-12">
                                <p>قیمت (۲ مورد):</p>
                            </div>
                            <div class="col-md-6 co-lg-6 col-sm-6 col-xs-12">
                                <p>۵۳۹,۰۰۰ ریال</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row well-l ">
                            <div class="col-md-6 co-lg-6 col-sm-6 col-xs-12">
                                <p>مجموع تخفیف:</p>
                            </div>
                            <div class="col-md-6 co-lg-6 col-sm-6 col-xs-12">
                                <p>۱,۲۱۰,۰۰۰ ریال</p>
                            </div>
                        </div>
                        <div class="row well-l ">
                            <div class="col-md-6 co-lg-6 col-sm-6 col-xs-12">
                                <p>مبلغ کل:</p>
                            </div>
                            <div class="col-md-6 co-lg-6 col-sm-6 col-xs-12">
                                <p class="bold">۵۳۹,۰۰۰ ریال</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row well-l icon">
                            <div class="col-md-9 co-lg-9 col-sm-9 col-xs-12">
                                <span class="check">۴۷۰,۰۰۰ ریال خرید بیشتر تا <span class="check-k">ارسال رایگان</span></span>
                            </div>
                            <div class="col-md-3 co-lg-3 col-sm-3 col-xs-12 text-right">
                                <span class="flaticon flaticon-delivery-truck"></span>
                            </div>
                            <div class="row a">
                                <a href="javascript:" class="" data-sort="1" style="">ثبت سفارشات</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row well-l text-center img">
                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                                <img src="image/return.png" alt="">
                                <br>
                                <p>تا۳۰روز بازگشت</p>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                                <img src="image/delivery-truck.png" alt="">
                                <br>
                                <p>ارسال رایگان</p>
                            </div>
                            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                                <img src="image/phone.png" alt="">
                                <br>
                                <p>پشتیبانی ۷/۲۴</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
                    <div class="well">
                        <div class="text-t">
                            <h5>سبد خرید</h5>
                        </div>
                        <div class="row ch-ek">
                            <div class="col-md-2">
                                <img src="image/6386_weide-watches.jpg" alt="">
                            </div>
                            <div class="col-md-6 pad-right">
                                <h5>weide</h5>
                                <h6>W311MI 150Mbps Wireless N USB Adapter</h6>
                                <br>
                                <br>
                                <span class="aK7u _8pWg" style="font-size: 0.75rem;">فروشنده:
                                    <a href="/avang-co">
                                        <span class="_1_5R _3rIw" style="font-size: 0.75rem;">Avang</span>
                                    </a>
                                </span>
                            </div>
                            <div class="col-md-4">
                                <div class="row icon-n text-center">
                                    <div class="col-md-6">
                                        <a href="">
                                            <img src="image/like.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="">
                                            <img src="image/error.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="row hs">
                                    <div class="col-md-6">
                                        <div class="container">
                                            <div class="count-input space-bottom">
                                                <a class="incr-btn" data-action="decrease" href="#">–</a>
                                                <input class="quantity" type="text" name="quantity" value="1"/>
                                                <a class="incr-btn" data-action="increase" href="#">&plus;</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <span>۲,۷۰۰,۰۰۰ ریال</span>
                                        <div class="price">۲,۴۴۰,۰۰۰ ریال</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row ch-ek">
                            <div class="col-md-2">
                                <img src="image/6386_weide-watches.jpg" alt="">
                            </div>
                            <div class="col-md-6 pad-right">
                                <h5>weide</h5>
                                <h6>W311MI 150Mbps Wireless N USB Adapter</h6>
                                <br>
                                <br>
                                <span class="aK7u _8pWg" style="font-size: 0.75rem;">فروشنده:
                                    <a href="/avang-co">
                                        <span class="_1_5R _3rIw" style="font-size: 0.75rem;">Avang</span>
                                    </a>
                                </span>
                            </div>
                            <div class="col-md-4">
                                <div class="row icon-n text-center">
                                    <div class="col-md-6">
                                        <a href="">
                                            <img src="image/like.png" alt="">
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="">
                                            <img src="image/error.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <div class="row hs">
                                    <div class="col-md-6">
                                        <div class="container">
                                            <div class="count-input space-bottom">
                                                <a class="incr-btn" data-action="decrease" href="#">–</a>
                                                <input class="quantity" type="text" name="quantity" value="1"/>
                                                <a class="incr-btn" data-action="increase" href="#">&plus;</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <span>۲,۷۰۰,۰۰۰ ریال</span>
                                        <div class="price">۲,۴۴۰,۰۰۰ ریال</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <br>
                    <div class="form">
                        <div class="row loc">
                            <div class="col-md-6">
                                <ul>
                                    <li class="brad">
                                        <a href="">مشخصات خریدار</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <form>
                            <div class="row">
                                <div class="col-md-4">
                                    <label>
                                        <p class="label-txt">نام*</p>
                                        <input type="text" class="input">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label>
                                        <p class="label-txt">نام خانوادگی*</p>
                                        <input type="text" class="input">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                                <div class="col-md-4">
                                    <label>
                                        <p class="label-txt">شماره همراه*</p>
                                        <input type="text" class="input">
                                        <div class="line-box">
                                            <div class="line"></div>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form-m">
                                                <div class="form-group ">
                                                    <label style="margin:2% 0%;font-size:.8em;color:rgb(120,120,120)" for="HowToKnow">استان و شهر*</label>
                                                    <div class="customselect">
                                                        <select class="form-control clgfocus">
                                                            <option value="">Select</option>
                                                            <option value="Newspaper">Newspaper</option>
                                                            <option value="Teacher">Teacher</option>
                                                            <option value="Friends/Relatives">Friends/Relatives</option>
                                                            <option value="Others">Others</option>
                                                        </select>
                                                    </div>
                                                    <span class="focus-border"></span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <form class="form-m">
                                                <div class="form-group ">
                                                    <label style="margin:2% 0%;font-size:.8em;color:rgb(120,120,120)" for="HowToKnow">محله</label>
                                                    <div class="customselect">
                                                        <select class="form-control clgfocus">
                                                            <option value="">Select</option>
                                                            <option value="Newspaper">Newspaper</option>
                                                            <option value="Teacher">Teacher</option>
                                                            <option value="Friends/Relatives">Friends/Relatives</option>
                                                            <option value="Others">Others</option>
                                                        </select>
                                                    </div>
                                                    <span class="focus-border"></span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <label>
                                <p class="label-txt">آدرس پستی</p>
                                <input type="text" class="input">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                            <a href="" class="sa" data-sort="1" style="">ثبت آدرس و ادامه</a>
                            <a href="" class="margin" data-sort="1" style="">انصراف</a>
                        </form>
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="form">
                        <div class="row loc">
                            <div class="col-md-6">
                                <ul>
                                    <li class="brad">
                                        <a href="">مشخصات خریدار</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-m">
                                    <div class="form-group ">
                                        <label style="margin:2% 0%;font-size:.8em;color:rgb(120,120,120)" for="HowToKnow">محله</label>
                                        <div class="customselect">
                                            <select class="form-control clgfocus">
                                                <option value="">Select</option>
                                                <option value="Newspaper">Newspaper</option>
                                                <option value="Teacher">Teacher</option>
                                                <option value="Friends/Relatives">Friends/Relatives</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                        <span class="focus-border"></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="form">
                        <div class="row loc">
                            <div class="col-md-6">
                                <ul>
                                    <li class="brad">
                                        <a href="">مشخصات خریدار</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-m">
                                    <div class="form-group ">
                                        <label style="margin:2% 0%;font-size:.8em;color:rgb(120,120,120)" for="HowToKnow">محله</label>
                                        <div class="customselect">
                                            <select class="form-control clgfocus">
                                                <option value="">Select</option>
                                                <option value="Newspaper">Newspaper</option>
                                                <option value="Teacher">Teacher</option>
                                                <option value="Friends/Relatives">Friends/Relatives</option>
                                                <option value="Others">Others</option>
                                            </select>
                                        </div>
                                        <span class="focus-border"></span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
    <?php include("blocks/footer.php");?>
    <?php include("blocks/script.php");?>

  </body>
</html>