// Set the date we're counting down to
var countDownDate = new Date();
// add a day
countDownDate.setDate(countDownDate.getDate() + 1);
// Update the count down every 1 second
var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();
    
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
    
    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
    if(days>0){
        hours = hours + (days*24);
        days = 0;
    }
    hours = zeroPad(hours,2);
    minutes = zeroPad(minutes,2);
    seconds = zeroPad(seconds,2);
    
    hours = hours.split('');
    minutes = minutes.split('');
    seconds = seconds.split('');
    // Output the result in an element with id="demo"

    jQuery('.hours_0').html(hours[0]);
    jQuery('.hours_1').html(hours[1]);
    jQuery('.minutes_0').html(minutes[0]);
    jQuery('.minutes_1').html(minutes[1]);
    jQuery('.seconds_0').html(seconds[0]);
    jQuery('.seconds_1').html(seconds[1]);
    // If the count down is over, write some text 
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
    }
}, 1000);

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}
/* Custom Select js */

var x, i, j, selElmnt, a, b, c;
x = document.getElementsByClassName("customselect");  /*look for any elements with the class "customselect":*/

for (i = 0; i < x.length; i++) {

	selElmnt = x[i].getElementsByTagName("select")[0];
	a = document.createElement("DIV");  /*for each element, create a new DIV that will act as the selected item:*/
	a.setAttribute("class", "select-selected");
	a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
	x[i].appendChild(a);

	b = document.createElement("DIV"); /*for each element, create a new DIV that will contain the option list:*/
	b.setAttribute("class", "select-items select-hide");

	for (j = 0; j < selElmnt.length; j++) {

		c = document.createElement("DIV"); /*for each option in the original select element, create a new DIV that will act as an option item:*/
		c.innerHTML = selElmnt.options[j].innerHTML;
		c.addEventListener("click", function (e) {
			/*when an item is clicked, update the original select box, and the selected item:*/
			var y, i, k, s, h;
			s = this.parentNode.parentNode.getElementsByTagName("select")[0];
			h = this.parentNode.previousSibling;

			for (i = 0; i < s.length; i++) {

				if (s.options[i].innerHTML == this.innerHTML) {
					s.selectedIndex = i;
					h.innerHTML = this.innerHTML;
					y = this.parentNode.getElementsByClassName("same-as-selected");
					for (k = 0; k < y.length; k++) {
						y[k].removeAttribute("class");
					}
					this.setAttribute("class", "same-as-selected");
					break;
				}
			}
			h.click();
		});
		b.appendChild(c);
	}
	x[i].appendChild(b);
	a.addEventListener("click", function (e) {
		/*when the select box is clicked, close any other select boxes,
		and open/close the current select box:*/
		e.stopPropagation();
		closeAllSelect(this);
		this.nextSibling.classList.toggle("select-hide");
		this.classList.toggle("select-arrow-active");
	});
}
function closeAllSelect(elmnt) {
	/*a function that will close all select boxes in the document,
	except the current select box:*/
	var x, y, i, arrNo = [];
	x = document.getElementsByClassName("select-items");
	y = document.getElementsByClassName("select-selected");
	for (i = 0; i < y.length; i++) {
		if (elmnt == y[i]) {
			arrNo.push(i)
		} else {
			y[i].classList.remove("select-arrow-active");
		}
	}
	for (i = 0; i < x.length; i++) {
		if (arrNo.indexOf(i)) {
			x[i].classList.add("select-hide");
		}
	}
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

/* Custom Select js end */
$(".incr-btn").on("click", function (e) {
	var $button = $(this);
	var oldValue = $button.parent().find('.quantity').val();
	$button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
	if ($button.data('action') == "increase") {
		var newVal = parseFloat(oldValue) + 1;
	} else {
		// Don't allow decrementing below 1
		if (oldValue > 1) {
			var newVal = parseFloat(oldValue) - 1;
		} else {
			newVal = 1;
			$button.addClass('inactive');
		}
	}
	$button.parent().find('.quantity').val(newVal);
	e.preventDefault();
});

$(document).ready(function() {
	var owl = $('.owl-carousel');
	owl.owlCarousel({
		rtl: true,
		margin: 10,
		nav: true,
		loop: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 3
			},
			1000: {
				items: 5
			}
		}
	})
})
 
/*megamenu*/ 

$(document).ready(function() {
	// executes when HTML-Document is loaded and DOM is ready
   // breakpoint and up  
   $(window).resize(function(){
	   if ($(window).width() >= 980){	
			// when you hover a toggle show its dropdown menu
			$(".navbar .dropdown-toggle").hover(function (){
				$(this).parent().toggleClass("show");
				$(this).parent().find(".dropdown-menu").toggleClass("show"); 
			});
			// hide the menu when the mouse leaves the dropdown
			$( ".navbar .dropdown-menu" ).mouseleave(function(){
				$(this).removeClass("show");  
			});
		   // do something here
	   }	
   });  
   // document ready  
});

/*megamenu*/

$(function(){
	var $a = $(".tabs li");
	$a.click(function(){
		$a.removeClass("active");
		$(this).addClass("active");
	});
});

var swiper = new Swiper('.swiper-container',{
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
});

$(function(){
	$(".search-box .dropdown-menu a").click(function(){
		var selText = $(this).html();
		$(this).parents().find('button.dropdown-toggle').html(selText);
	});
});



/*slider_2*/

$('#myCarousel').carousel({
    interval: 3000,
 })

/* product-details*/

$(document).ready(function() {
    $('body').bootstrapMaterialDesign();
    
    
});




$(document).ready(function(){
	$('.count').prop('disabled', true);
	   $(document).on('click','.plus',function(){
		$('.count').val(parseInt($('.count').val()) + 1 );
	});
	$(document).on('click','.minus',function(){
		$('.count').val(parseInt($('.count').val()) - 1 );
			if ($('.count').val() == 0) {
				$('.count').val(1);
			}
		});
 });


 $(document).ready(function(){

	$('input').focus(function(){
	  $(this).parent().find(".label-txt").addClass('label-active');
	});
  
	$("input").focusout(function(){
	  if ($(this).val() == '') {
		$(this).parent().find(".label-txt").removeClass('label-active');
	  };
	});
  
  });

  