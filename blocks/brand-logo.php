<div class="brand-logo">
    <div class="container">
        <br>
        <h4 class="h4">برندها</h4>
        <hr>
        <div class="owl-carousel owl-theme">
            <div class="item">
                <img src="image/brand-1.png" alt="">
            </div>
            <div class="item">
                <img src="image/brand-2.png" alt="">
            </div>
            <div class="item">
                <img src="image/brand-3.png" alt="">
            </div>
            <div class="item">
                <img src="image/brand-4.png" alt="">
            </div>
            <div class="item">
                <img src="image/brand-5.png" alt="">
            </div>
            <div class="item">
                <img src="image/brand-6.png" alt="">
            </div>
        </div>
    </div>
</div>