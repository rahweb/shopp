<div class="services">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-6 right-box">
                <a href="">
                    <span class="flaticon flaticon-menu"></span>
                </a>
                <p>خدمات پس از فروش</p>
            </div>
            <div class="col-md-3 col-6 right-box">
                <a href="">
                    <span class="flaticon flaticon-payment-method"></span>
                </a>
                <p>پرداخت در محل</p>
            </div>
            <div class="col-md-3 col-6 col">
                <a href="">
                    <span class="flaticon flaticon-delivery-truck"></span>
                </a>
                <p>ارسال به تمام نقاط کشور</p>
            </div>
            <div class="col-md-3 col-6 left-box">
                <a href="">
                    <span class="flaticon flaticon-telephone-auricular-with-cable"></span>
                </a>
                <p>پشتیبانی ۲۴ ساعته</p>
            </div>
        </div>
    </div>
</div>