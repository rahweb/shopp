<div class="list-t-product">
    <div class="container">
        <br>
        <h4 class="h4">محصولات - ۱</h4>
        <hr>
        <div class="row list-hs">
            <div class="fff">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="">
                        <div class="title text-center">
                            <h6>لیست</h6>
                        </div>
                        <br>
                        <div class="list">
                            <div class="cat_slider_inner so_category_type_default">
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                            <a href="#" title="تستی" target="_self">تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                            <a href="#" title="تستی" target="_self">تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                            <a href="#" title="تستی &amp; تستی" target="_self">تستی &amp; تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                        <a href="#" title="تستی" target="_self">تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                            <a href="#" title="تستی" target="_self">تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                            <a href="#" title="تستی &amp; تستی" target="_self">تستی &amp; تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                            <a href="#" title="تستی &amp; تستی" target="_self">تستی &amp; تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                        <a href="#" title="تستی" target="_self">تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                        <a href="#" title="تستی" target="_self">تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                        <a href="#" title="تستی &amp; تستی" target="_self">تستی &amp; تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="item-inner">
                                        <div class="cat_slider_title">
                                        <a href="#" title="تستی &amp; تستی" target="_self">تستی &amp; تستی</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="view-all"><a href="#">مشاهده همه</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <div class="spc-wrap">
                    <div class="">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="#">
                                            <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-1.jpg">
                                            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-2.jpg">
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                            <li><a href="#" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                        </ul>
                                        <span class="product-new-label">جدید</span>
                                        <span class="product-discount-label">-۱۰٪</span>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a href="#">نام محصول</a></h3>
                                        <div class="price">
                                            ۱,۲۳۴,۰۰۰ریال
                                            <span>۱,۳۵۰,۰۰۰ریال</span>
                                        </div>
                                        <!-- <a class="add-to-cart" href="">افزودن به سبد خرید</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="#">
                                            <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-3.jpg">
                                            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-4.jpg">
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                            <li><a href="#" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                        </ul>
                                        <span class="product-new-label">جدید</span>
                                        <span class="product-discount-label">-۱۰٪</span>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a href="#">نام محصول</a></h3>
                                        <div class="price">
                                            ۱,۲۳۴,۰۰۰ریال
                                            <span>۱,۳۵۰,۰۰۰ریال</span>
                                        </div>
                                        <!-- <a class="add-to-cart" href="">افزودن به سبد خرید</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="#">
                                            <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-5.jpg">
                                            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-6.jpg">
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                            <li><a href="#" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                        </ul>
                                        <span class="product-new-label">جدید</span>
                                        <span class="product-discount-label">-۱۰٪</span>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a href="#">نام محصول</a></h3>
                                        <div class="price">
                                            ۱,۲۳۴,۰۰۰ریال
                                            <span>۱,۳۵۰,۰۰۰ریال</span>
                                        </div>
                                        <!-- <a class="add-to-cart" href="">افزودن به سبد خرید</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="#">
                                            <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-7.jpg">
                                            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-8.jpg">
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                            <li><a href="#" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                        </ul>
                                        <span class="product-new-label">جدید</span>
                                        <span class="product-discount-label">-۱۰٪</span>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a href="#">نام محصول</a></h3>
                                        <div class="price">
                                            ۱,۲۳۴,۰۰۰ریال
                                            <span>۱,۳۵۰,۰۰۰ریال</span>
                                        </div>
                                        <!-- <a class="add-to-cart" href="">افزودن به سبد خرید</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="#">
                                            <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-1.jpg">
                                            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-2.jpg">
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                            <li><a href="#" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                        </ul>
                                        <span class="product-new-label">جدید</span>
                                        <span class="product-discount-label">-۱۰٪</span>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a href="#">نام محصول</a></h3>
                                        <div class="price">
                                            ۱,۲۳۴,۰۰۰ریال
                                            <span>۱,۳۵۰,۰۰۰ریال</span>
                                        </div>
                                        <!-- <a class="add-to-cart" href="">افزودن به سبد خرید</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="#">
                                            <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-3.jpg">
                                            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-4.jpg">
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                            <li><a href="#" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                        </ul>
                                        <span class="product-new-label">جدید</span>
                                        <span class="product-discount-label">-۱۰٪</span>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a href="#">نام محصول</a></h3>
                                        <div class="price">
                                            ۱,۲۳۴,۰۰۰ریال
                                            <span>۱,۳۵۰,۰۰۰ریال</span>
                                        </div>
                                        <!-- <a class="add-to-cart" href="">افزودن به سبد خرید</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="#">
                                            <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-5.jpg">
                                            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-6.jpg">
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                            <li><a href="#" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                        </ul>
                                        <span class="product-new-label">جدید</span>
                                        <span class="product-discount-label">-۱۰٪</span>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a href="#">نام محصول</a></h3>
                                        <div class="price">
                                            ۱,۲۳۴,۰۰۰ریال
                                            <span>۱,۳۵۰,۰۰۰ریال</span>
                                        </div>
                                        <!-- <a class="add-to-cart" href="">افزودن به سبد خرید</a> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="product-grid4">
                                    <div class="product-image4">
                                        <a href="#">
                                            <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-7.jpg">
                                            <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo5/images/img-8.jpg">
                                        </a>
                                        <ul class="social">
                                            <li><a href="#" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                            <li><a href="#" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                        </ul>
                                        <span class="product-new-label">جدید</span>
                                        <span class="product-discount-label">-۱۰٪</span>
                                    </div>
                                    <div class="product-content">
                                        <h3 class="title"><a href="#">نام محصول</a></h3>
                                        <div class="price">
                                            ۱,۲۳۴,۰۰۰ریال
                                            <span>۱,۳۵۰,۰۰۰ریال</span>
                                        </div>
                                        <!-- <a class="add-to-cart" href="">افزودن به سبد خرید</a> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>