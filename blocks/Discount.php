<div class="card">
    <div class="container">
        <br>
        <h3 class="text">محصولات تخفیف دار</h3>
        <hr>
        <div class="cardbox section">
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="100000">
                <div class="float-right navi">
                    <a class="" href="#carouselExampleControls" role="button" data-slide="prev">
                            <img src="image/next.png" alt="">
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="" href="#carouselExampleControls" role="button" data-slide="next">
                            <img src="image/back.png" alt="">
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="w-100 carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <div class="carousel-caption">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                    <img src="image/termeh-negar-2136-4601394-1-catalog.jpg" alt="" class="rounded-circle img-fluid"/>
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                                    <h5>360,000</h5>
                                    <h4>270,000</h4>
                                    <h3>محصول-1</h3>
                                    <ul>
                                        <li>نوع:</li>
                                        <li>مدل:</li>
                                    </ul>
                                    <hr>
                                    <h2>10:49:53</h2>
                                    <p>زمان باقی مانده تاپایان سفارش</p>
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                                    <br>
                                    <h3 class="Percentage">۹٪ تخفیف</h3>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <button type="button" class="btn btn-success">خرید محصول</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-caption">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                    <img src="image/charme-khatere-7089-9415445-1-catalog.jpg" alt="" class="rounded-circle img-fluid"/>
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                                    <h5>360,000</h5>
                                    <h4>270,000</h4>
                                    <h3>محصول-1</h3>
                                    <ul>
                                        <li>نوع:</li>
                                        <li>مدل:</li>
                                    </ul>
                                    <hr>
                                    <h2>10:49:53</h2>
                                    <p>زمان باقی مانده تاپایان سفارش</p>
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                                    <br>
                                    <br>
                                    <h3 class="Percentage">۹٪ تخفیف</h3>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <button type="button" class="btn btn-success">خرید محصول</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-caption">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                    <img src="image/termeh-negar-2136-4601394-1-catalog.jpg" alt="" class="rounded-circle img-fluid"/>
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                                    <h5>360,000</h5>
                                    <h4>270,000</h4>
                                    <h3>محصول-1</h3>
                                    <ul>
                                        <li>نوع:</li>
                                        <li>مدل:</li>
                                    </ul>
                                    <hr>
                                    <h2>10:49:53</h2>
                                    <p>زمان باقی مانده تاپایان سفارش</p>
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                                    <br>
                                    <h3 class="Percentage">۹٪ تخفیف</h3>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <button type="button" class="btn btn-success">خرید محصول</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-caption">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                    <img src="image/charme-khatere-7089-9415445-1-catalog.jpg" alt="" class="rounded-circle img-fluid"/>
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                                    <h5>360,000</h5>
                                    <h4>270,000</h4>
                                    <h3>محصول-1</h3>
                                    <ul>
                                        <li>نوع:</li>
                                        <li>مدل:</li>
                                    </ul>
                                    <hr>
                                    <h2>10:49:53</h2>
                                    <p>زمان باقی مانده تاپایان سفارش</p>
                                </div>
                                <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                                    <br>
                                    <h3 class="Percentage">۹٪ تخفیف</h3>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <button type="button" class="btn btn-success">خرید محصول</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>