<div class="share-footer text-center">
    <div class="container">
        <div class="row menu-footer">
            <div class="col-md-3">
                <a href=""><img src="image/tel.png" alt=""></a>
            </div>
            <div class="col-md-3">
                <a href=""><img src="image/insta.png" alt=""></a>
            </div>
            <div class="col-md-3 app ">
                <a href=""><img src="image/app.png" alt=""></a>
            </div>
            <div class="col-md-3 app">
                <a href=""><img src="image/bazar.png" alt=""></a>
            </div>
        </div>
    </div>
</div>
<div class="footer">
    <br>
    <div class="container">        
        <div class="row topfooter">
            <div class="col-md-4 col-12">
                <ul>
                    <li>
                        <h4>خدمات مشتریان</h4>
                    </li>
                    <hr>
                    <li>
                        <a href=""><p>سوالات متداول</p></a>
                    </li>
                    <li>
                        <a href=""><p>راهنمای انتخاب سایز</p></a>
                    </li>
                    <li>
                        <a href=""><p>شرایط حمل و تحویل کالا</p></a>
                    </li>
                    <li>
                        <a href=""><p>شرایط استفاده از کوپن تخفیف</p></a>
                    </li>
                    <li>
                        <a href=""><p>سیاست حفظ حریم خصوصی</p></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>
                        <h4>درباره ما</h4>
                    </li>
                    <hr>
                    <li>
                        <a href=""><p>درباره ما</p></a>
                    </li>
                    <li>
                        <a href=""><p>تماس با ما</p></a>
                    </li>
                    <li>
                        <a href=""><p>همکاری با ما</p></a>
                    </li>
                    <li>
                        <a href=""><p>فروش عمده و سازمانی</p></a>
                    </li>
                    <li>
                        <a href=""><p>ثبت نام تامین کنندگان</p></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 col-12">
                <br>
                <br>
                <div class="row">
                    <div class="col-md-6 col-6">
                        <a href=""><img src="image/namad.png" alt=""></a>
                    </div>
                    <div class="col-md-6 col-6">
                        <a href=""><img src="image/samandehi.png" alt=""></a>
                    </div>
                </div>
                <br>
                <!-- <div class="row bank">
                    <div class="col-md-4">
                        <a href="">
                            <img src="image/Maskan.png" alt="">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="">
                            <img src="image/Approximated.png" alt="">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="">
                            <img src="image/Sepah.png" alt="">
                        </a>
                    </div>
                </div> -->
            </div>
        </div>
        <hr>
        <div class="row icon">
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_01_111032.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_google__b_104841.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_66_Instagram_106164.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_linkdin__social_media_logo_2986200.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_telegram_401554.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_twitter_401562.png" alt="">
                </a>
            </div>
        </div>
        <hr>
        <section style="text-align:center; margin:10px auto;">
            <p>طراحی و پیاده سازی سایت : <a href="http://rahweb.ir/">گروه طراحان ره وب</a></p>
        </section>
        <br>
    </div>
</div>