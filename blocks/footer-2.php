<div class="share-footer text-center">
    <div class="container">
        <div class="row menu-footer">
            <div class="col-md-3">
                <a href=""><img src="image/tel.png" alt=""></a>
            </div>
            <div class="col-md-3">
                <a href=""><img src="image/insta.png" alt=""></a>
            </div>
            <div class="col-md-3 app ">
                <a href=""><img src="image/app.png" alt=""></a>
            </div>
            <div class="col-md-3 app">
                <a href=""><img src="image/bazar.png" alt=""></a>
            </div>
        </div>
    </div>
</div>
<footer class="footer-2">
    <div class="container bottom_border">
        <div class="row">
            <div class=" col-sm-4 col-md col-sm-4  col-12 col">
                <h5 class="headin5_amrc col_white_amrc pt2">آدرس ما</h5>
                <!--headin5_amrc-->
                <p class="mb10">تهران- میدان حسن آباد- خیابان امام خمینی (ره)- بین تقاطع شیخ هادی و ولیعصر- بازار آنلاین پارسیان </p>
                <p><i class="fa fa-location-arrow"></i> 9878/25 sec 9 rohini 35 </p>
                <p><i class="fa fa-phone"></i>021-12345678</p>
                <p><i class="fa fa fa-envelope"></i>info@gmail.com</p>
            </div>
            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">خدمات مشتریان</h5>
                <!--headin5_amrc-->
                <ul class="footer_ul_amrc">
                    <li><a href="#">سوالات متداول</a></li>
                    <li><a href="#">راهنمای انتخاب سایز</a></li>
                    <li><a href="#">شرایط حمل و تحویل کالا</a></li>
                    <li><a href="#">شرایط استفاده از کوپن تخفیف</a></li>
                    <li><a href="#">سیاست حفظ حریم خصوصی</a></li>
                </ul>
                <!--footer_ul_amrc ends here-->
            </div>
            <div class=" col-sm-4 col-md  col-6 col">
                <h5 class="headin5_amrc col_white_amrc pt2">درباره ما</h5>
                <!--headin5_amrc-->
                <ul class="footer_ul_amrc">
                    <li><a href="#">همکاری با ما</a></li>
                    <li><a href="#">فروش عمده و سازمانی</a></li>
                    <li><a href="#">ثبت نام تامین کنندگان</a></li>
                    <li><a href="#">تماس با ما</a></li>
                </ul>
                <!--footer_ul_amrc ends here-->
            </div>
            <div class=" col-sm-4 col-md  col-12 col">
                <h5 class="headin5_amrc col_white_amrc pt2"></h5>
                <!--headin5_amrc ends here-->
                <div class="row">
                    <div class="col-md-6 col-6">
                        <a href=""><img src="image/namad.png" alt=""></a>
                    </div>
                    <div class="col-md-6 col-6">
                        <a href=""><img src="image/samandehi.png" alt=""></a>
                    </div>
                </div>
                <!--footer_ul2_amrc ends here-->
            </div>
        </div>
    </div>
    <div class="container">
        <ul class="foote_bottom_ul_amrc">
            <li><a href="#">خانه</a></li>
            <li><a href="#">درباره ما</a></li>
            <li><a href="#">خدمات</a></li>
            <li><a href="#">تماس باما</a></li>
        </ul>
        <!--foote_bottom_ul_amrc ends here-->
        <div class="row icon text-center">
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_01_111032.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_google__b_104841.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_66_Instagram_106164.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_linkdin__social_media_logo_2986200.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_telegram_401554.png" alt="">
                </a>
            </div>
            <div class="col-md-2 col-xl-2">
                <a href="">
                    <img src="image/if_twitter_401562.png" alt="">
                </a>
            </div>
        </div>
        <p class="text-center rahweb">طراحی و پیاده سازی سایت : <a href="#">گروه طراحان ره وب</a></p>
        <br>
        <!--social_footer_ul ends here-->
    </div>
</footer>