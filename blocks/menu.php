<div class="topmenu">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-xs-12 rightbox">
                <div class="">
                    <div class="col-md-3 col-xs-4">
                        <a href="">
                            <img src="image/england.png" alt="">
                        </a>
                    </div>
                    <div class="col-md-3 col-xs-4">
                        <a href="">
                            <img src="image/iran(1).png" alt="">
                        </a>
                    </div>
                    <div class="col-md-3 col-xs-4">
                        <a href="">
                            <img src="image/saudi-arabia.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-xs-6">
                <ul>
                    <li>
                        <a href=""><span class="flaticon flaticon-gmail-logo"></span></a>
                    </li>
                    <li>
                        <a href=""><p>info@gmail.com</p></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 col-xs-6 centerbox">
                <ul>
                    <li>
                        <a href=""><span class="flaticon flaticon-telephone-of-old-design"></span></a>
                    </li>
                    <li>
                        <a href=""><p>021 - 22334455</p></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 col-xs-12 leftbox">
                <p>سه شنبه - ۱۵ آبان ۱۳۹۷</p>
            </div>
        </div>
    </div>
</div>
<div class="header">
    <div class="container">
        <div class="row centermenu">
            <div class="col-md-3 col-12">
                <a href=""><img src="image/logo.png" alt=""></a>
            </div>
            <div class="col-md-5 col-12">
                <div class="container top-nav">
                    <div class="row">                            
                        <div class="search-box input-group m-3">
                            <div class="input-group-prepend">
                                <button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">تمام محصولات</button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">صفحه ۱</a>
                                    <a class="dropdown-item" href="#">صفحه ۲</a>
                                    <a class="dropdown-item" href="#">صفحه ۳</a>
                                </div>
                            </div>
                            <input type="text" class="form-control" aria-label="Text input with dropdown button">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button">
                                    <img src="image/search.png" alt="">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12 left">
                <div class="col-md-4 col-4">
                    <div class="">
                        <div class="dropdown shop">
                            <button class="btn btn-primary dropdown-toggle grey-2" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="image/shopping-cart.png" alt=""> سبد خرید
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <div class="row you">
                                    <div class="col-md-3 img">
                                        <img src="image/qqqq.jpg" alt="">
                                    </div>
                                    <div class="col-md-9 text">
                                        <div class="">
                                            <div class="col-md-9">
                                                <div class="title">
                                                    <p>نام محصول</p>
                                                    <p>123,000</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 padding">
                                                <div class="close">
                                                    <a href=""><img src="image/error.png" alt=""></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row factor">
                                    <div class="col-md-6">
                                        <h6>جمع:</h6>
                                        <hr>
                                        <h6>مالیات:</h6>
                                        <hr>
                                        <!-- <h6>حمل ونقل:</h6>
                                        <hr> -->
                                        <h6>مبلغ پرداختی:</h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6>۱,۲۳۹,۰۰۰ ریال</h6>
                                        <hr>
                                        <h6>۱۱۵,۰۰۰ ریال</h6>
                                        <hr>
                                        <!-- <h6>۰ ریال</h6>
                                        <hr> -->
                                        <h6>۱,۳۵۴,۰۰۰ ریال</h6>
                                    </div>
                                    <hr>
                                    <button type="button" class="btn btn-info grey-1">نمایش سبد خرید و پرداخت</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-4">
                    <button type="button" class="btn btn-info grey-1"><img src="image/in.png" alt="">  ورود</button>
                </div>
                <div class="col-md-4 col-4">
                    <button type="button" class="btn btn-info grey-1"><img src="image/login.png" alt="">  ثبت نام</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="megamenu">
    <div class="">
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">خانه</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                محصولات 1
                            </a>
                            <div class="dropdown-menu mega" aria-labelledby="navbarDropdown">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href="">
                                                <img src="image/6386_weide-watches.jpg" alt="" class="img-fluid">
                                            </a>
                                            <br>
                                            <p class="text-white">ساعت های برند weide</p>
                                        </div>
                                        <!-- /.col-md-3  -->
                                    </div>
                                </div>
                                <!--  /.container  -->
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                محصولات 2
                            </a>
                            <div class="dropdown-menu mega" aria-labelledby="navbarDropdown">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href="">
                                                <img src="image/6386_weide-watches.jpg" alt="" class="img-fluid">
                                            </a>
                                            <br>
                                            <p class="text-white">ساعت های برند weide</p>
                                        </div>
                                        <!-- /.col-md-3  -->
                                    </div>
                                </div>
                                <!--  /.container  -->
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                محصولات 3
                            </a>
                            <div class="dropdown-menu mega" aria-labelledby="navbarDropdown">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href=""><span class="text-uppercase text-white"><img src="image/back.png" alt="">لوازم جانبی گوشی</span></a>
                                            <br>
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a class="nav-link active" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">کیف و کاور گوشی</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">هندزفری، هدفون، هدست</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" href="#">پایه نگهدارنده گوشی</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- /.col-md-3  -->
                                        <div class="col-md-3">
                                            <a href="">
                                                <img src="image/6386_weide-watches.jpg" alt="" class="img-fluid">
                                            </a>
                                            <br>
                                            <p class="text-white">ساعت های برند weide</p>
                                        </div>
                                        <!-- /.col-md-3  -->
                                    </div>
                                </div>
                                <!--  /.container  -->
                            </div>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="#">تماس باما</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="#">درباره ما</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <a class="navbar-brand" href="#"></a>
                    </form>
                </div>
            </div>
        </nav>
    </div>
</div>