<div class="products">
    <div class="container">
        <br>
        <h4 class="h4">محصولات - ۱</h4>
        <hr>
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="product">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#">
                                <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-5.jpg">
                                <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-6.jpg">
                            </a>
                            <ul class="social">
                                <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                <li><a href="" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                            </ul>
                            <span class="product-new-label">تخفیف</span>
                            <span class="product-discount-label">20%</span>
                        </div>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                        <div class="product-content">
                            <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                            <div class="price">۲,۴۴۰,۰۰۰ ریال
                                <span>۲,۷۰۰,۰۰۰ ریال</span>
                            </div>
                            <a class="add-to-cart" href="">+ افزودن به سبد خرید</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="product">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#">
                                <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-7.jpg">
                                <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-8.jpg">
                            </a>
                            <ul class="social">
                                <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                <li><a href="" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                            </ul>
                            <span class="product-new-label">تخفیف</span>
                            <span class="product-discount-label">20%</span>
                        </div>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                        <div class="product-content">
                            <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                            <div class="price">۲,۴۴۰,۰۰۰ ریال
                                <span>۲,۷۰۰,۰۰۰ ریال</span>
                            </div>
                            <a class="add-to-cart" href="">+ افزودن به سبد خرید</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="product">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#">
                                <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                                <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                            </a>
                            <ul class="social">
                                <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                <li><a href="" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                            </ul>
                            <span class="product-new-label">تخفیف</span>
                            <span class="product-discount-label">20%</span>
                        </div>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                        <div class="product-content">
                            <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                            <div class="price">۲,۴۴۰,۰۰۰ ریال
                                <span>۲,۷۰۰,۰۰۰ ریال</span>
                            </div>
                            <a class="add-to-cart" href="">+ افزودن به سبد خرید</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="product">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#">
                                <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-3.jpg">
                                <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-4.jpg">
                            </a>
                            <ul class="social">
                                <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                <li><a href="" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                            </ul>
                            <span class="product-new-label">تخفیف</span>
                            <span class="product-discount-label">20%</span>
                        </div>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                        <div class="product-content">
                            <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                            <div class="price">۲,۴۴۰,۰۰۰ ریال
                                <span>۲,۷۰۰,۰۰۰ ریال</span>
                            </div>
                            <a class="add-to-cart" href="">+ افزودن به سبد خرید</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="product">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#">
                                <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-5.jpg">
                                <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-6.jpg">
                            </a>
                            <ul class="social">
                                <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                <li><a href="" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                            </ul>
                            <span class="product-new-label">تخفیف</span>
                            <span class="product-discount-label">20%</span>
                        </div>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                        <div class="product-content">
                            <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                            <div class="price">۲,۴۴۰,۰۰۰ ریال
                                <span>۲,۷۰۰,۰۰۰ ریال</span>
                            </div>
                            <a class="add-to-cart" href="">+ افزودن به سبد خرید</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="product">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#">
                                <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-7.jpg">
                                <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-8.jpg">
                            </a>
                            <ul class="social">
                                <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                <li><a href="" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                            </ul>
                            <span class="product-new-label">تخفیف</span>
                            <span class="product-discount-label">20%</span>
                        </div>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                        <div class="product-content">
                            <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                            <div class="price">۲,۴۴۰,۰۰۰ ریال
                                <span>۲,۷۰۰,۰۰۰ ریال</span>
                            </div>
                            <a class="add-to-cart" href="">+ افزودن به سبد خرید</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="product">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#">
                                <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                                <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                            </a>
                            <ul class="social">
                                <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                <li><a href="" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                            </ul>
                            <span class="product-new-label">تخفیف</span>
                            <span class="product-discount-label">20%</span>
                        </div>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                        <div class="product-content">
                            <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                            <div class="price">۲,۴۴۰,۰۰۰ ریال
                                <span>۲,۷۰۰,۰۰۰ ریال</span>
                            </div>
                            <a class="add-to-cart" href="">+ افزودن به سبد خرید</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="product">
                    <div class="product-grid">
                        <div class="product-image">
                            <a href="#">
                                <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-3.jpg">
                                <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-4.jpg">
                            </a>
                            <ul class="social">
                                <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                <li><a href="" data-tip="افزودن به سبد خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                            </ul>
                            <span class="product-new-label">تخفیف</span>
                            <span class="product-discount-label">20%</span>
                        </div>
                        <ul class="rating">
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                            <li class="fa fa-star"></li>
                        </ul>
                        <div class="product-content">
                            <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                            <div class="price">۲,۴۴۰,۰۰۰ ریال
                                <span>۲,۷۰۰,۰۰۰ ریال</span>
                            </div>
                            <a class="add-to-cart" href="">+ افزودن به سبد خرید</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>