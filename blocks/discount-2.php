<div class="discount-2 card">
    <div class="row" style="width:100%;margin: auto;">
        <div class="col-md-3 img">
            <a href=""><img src="image/4575.jpg" alt=""></a>
        </div>
        <div class="col-md-9">
            <div class="cardbox section">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" data-interval="100000">
                    <div class="float-right navi">
                        <a class="" href="#carouselExampleControls" role="button" data-slide="prev">
                            <img src="image/next.png" alt="">
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="" href="#carouselExampleControls" role="button" data-slide="next">
                            <img src="image/back.png" alt="">
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                    <div class="w-100 carousel-inner" role="listbox">
                        <div class="carousel-item active">
                            <div class="carousel-caption">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                        <img src="image/6386_weide-watches.jpg" alt="" class="rounded-circle img-fluid"/>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-4">
                                        <br>
                                        <br>
                                        <h5>360,000</h5>
                                        <h4>270,000</h4>
                                        <h3>محصول-1</h3>
                                        <ul>
                                            <li>نوع:</li>
                                            <li>مدل:</li>
                                        </ul>
                                        <hr>
                                        <div class="row timer">
                                            <div class="seconds parent_time">
                                                <div class="seconds_0 child_time child_time0" ></div>
                                                <div class="seconds_1 child_time child_time1" ></div>
                                            </div>
                                            <div class="minutes parent_time">
                                                <div class="minutes_0 child_time child_time0" ></div>
                                                <div class="minutes_1 child_time child_time1" ></div>
                                            </div>
                                            <div class="hours parent_time">
                                                <div class="hours_0 child_time child_time0" ></div>
                                                <div class="hours_1 child_time child_time1" ></div>
                                            </div>
                                        </div>
                                        <p>زمان باقی مانده تاپایان سفارش</p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-2">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <h4 class="Percentage-1">پیشنهاد شگفت انگیز</h4>
                                        <br>
                                        <h4 class="Percentage">۹٪ تخفیف</h4>
                                        <br>
                                        <button type="button" class="btn btn-success">خرید محصول</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="carousel-caption">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                        <img src="image/6386_weide-watches.jpg" alt="" class="rounded-circle img-fluid"/>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-4">
                                        <br>
                                        <br>
                                        <h5>360,000</h5>
                                        <h4>270,000</h4>
                                        <h3>محصول-1</h3>
                                        <ul>
                                            <li>نوع:</li>
                                            <li>مدل:</li>
                                        </ul>
                                        <hr>
                                        <div class="row timer">
                                            <div class="seconds parent_time">
                                                <div class="seconds_0 child_time child_time0" ></div>
                                                <div class="seconds_1 child_time child_time1" ></div>
                                            </div>
                                            <div class="minutes parent_time">
                                                <div class="minutes_0 child_time child_time0" ></div>
                                                <div class="minutes_1 child_time child_time1" ></div>
                                            </div>
                                            <div class="hours parent_time">
                                                <div class="hours_0 child_time child_time0" ></div>
                                                <div class="hours_1 child_time child_time1" ></div>
                                            </div>
                                        </div>
                                        <p>زمان باقی مانده تاپایان سفارش</p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-2">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <h4 class="Percentage-1">پیشنهاد شگفت انگیز</h4>
                                        <br>
                                        <h4 class="Percentage">۹٪ تخفیف</h4>
                                        <br>
                                        <button type="button" class="btn btn-success">خرید محصول</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="carousel-caption">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                        <img src="image/6386_weide-watches.jpg" alt="" class="rounded-circle img-fluid"/>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-4">
                                        <br>
                                        <br>
                                        <h5>360,000</h5>
                                        <h4>270,000</h4>
                                        <h3>محصول-1</h3>
                                        <ul>
                                            <li>نوع:</li>
                                            <li>مدل:</li>
                                        </ul>
                                        <hr>
                                        <div class="row timer">
                                            <div class="seconds parent_time">
                                                <div class="seconds_0 child_time child_time0" ></div>
                                                <div class="seconds_1 child_time child_time1" ></div>
                                            </div>
                                            <div class="minutes parent_time">
                                                <div class="minutes_0 child_time child_time0" ></div>
                                                <div class="minutes_1 child_time child_time1" ></div>
                                            </div>
                                            <div class="hours parent_time">
                                                <div class="hours_0 child_time child_time0" ></div>
                                                <div class="hours_1 child_time child_time1" ></div>
                                            </div>
                                        </div>
                                        <p>زمان باقی مانده تاپایان سفارش</p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-2">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <h4 class="Percentage-1">پیشنهاد شگفت انگیز</h4>
                                        <br>
                                        <h4 class="Percentage">۹٪ تخفیف</h4>
                                        <br>
                                        <button type="button" class="btn btn-success">خرید محصول</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="carousel-caption">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                                        <img src="image/6386_weide-watches.jpg" alt="" class="rounded-circle img-fluid"/>
                                    </div>
                                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-4">
                                        <br>
                                        <br>
                                        <h5>360,000</h5>
                                        <h4>270,000</h4>
                                        <h3>محصول-1</h3>
                                        <ul>
                                            <li>نوع:</li>
                                            <li>مدل:</li>
                                        </ul>
                                        <hr>
                                        <div class="row timer">
                                            <div class="seconds parent_time">
                                                <div class="seconds_0 child_time child_time0" ></div>
                                                <div class="seconds_1 child_time child_time1" ></div>
                                            </div>
                                            <div class="minutes parent_time">
                                                <div class="minutes_0 child_time child_time0" ></div>
                                                <div class="minutes_1 child_time child_time1" ></div>
                                            </div>
                                            <div class="hours parent_time">
                                                <div class="hours_0 child_time child_time0" ></div>
                                                <div class="hours_1 child_time child_time1" ></div>
                                            </div>
                                        </div>
                                        <p>زمان باقی مانده تاپایان سفارش</p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xs-12 col-sm-2">
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <h4 class="Percentage-1">پیشنهاد شگفت انگیز</h4>
                                        <br>
                                        <h4 class="Percentage">۹٪ تخفیف</h4>
                                        <br>
                                        <button type="button" class="btn btn-success">خرید محصول</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>