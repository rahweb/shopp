<div class="slider_2">
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="mask flex-center">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-md-7 col-12 order-md-1 order-2">
                                <h4>بولیزهای تابستانی
                                    <hr>
                                شیک وراحت</h4>
                                <p>الیاف لطیف پنبه ‏(‏Cotton‏)‏ هادی حرارت هستند، حرارت بدن به بیرون منتقل
                                    کرده و در نتیجه خنک هستند‏.‏ لباس های پنبه ای به راحتی لکه گیری و شسته
                                    می شوند و نسبت به الیاف مصنوعی وزن بیشتری دارند</p>
                                <!-- <a href="#">خرید</a> -->
                            </div>
                            <div class="col-md-5 col-12 order-md-2 order-1">
                                <img src="image/model-0.png" class="mx-auto" alt="slide">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="mask flex-center">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-md-7 col-12 order-md-1 order-2">
                                <h4>تیشرت ساده مردانه</h4>
                                <hr>
                                <p>الیاف لطیف پنبه ‏(‏Cotton‏)‏ هادی حرارت هستند، حرارت بدن به بیرون منتقل
                                    کرده و در نتیجه خنک هستند‏.‏ لباس های پنبه ای به راحتی لکه گیری و شسته
                                    می شوند و نسبت به الیاف مصنوعی وزن بیشتری دارند</p>
                                <!-- <a href="#">خرید</a> -->
                            </div>
                            <div class="col-md-5 col-12 order-md-2 order-1">
                                <img src="image/model-1.png" class="mx-auto" alt="slide">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="mask flex-center">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-md-7 col-12 order-md-1 order-2">
                                <h4>تیشرت ساده مردانه
                                    <hr>
                                پنبه ای</h4>
                                <p>الیاف لطیف پنبه ‏(‏Cotton‏)‏ هادی حرارت هستند، حرارت بدن به بیرون منتقل
                                    کرده و در نتیجه خنک هستند‏.‏ لباس های پنبه ای به راحتی لکه گیری و شسته
                                    می شوند و نسبت به الیاف مصنوعی وزن بیشتری دارند</p>
                                <!-- <a href="#">خرید</a> -->
                            </div>
                            <div class="col-md-5 col-12 order-md-2 order-1">
                                <img src="image/model-4.png" class="mx-auto" alt="slide">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!--slide end--> 
</div>