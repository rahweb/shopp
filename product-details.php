<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/menu.php");?>
    <div class="product-details">
        <br>
        <div class="container">
            <div class="row loc">
                <div class="col-md-6">
                    <ul>
                        <li class="home">
                            <a href=""><span class="flaticon flaticon-home" style="color:#ec5598"></span>فروشگاه اینترنتی</a>
                        </li>
                        <li>
                            <a href="">محصولات</a>
                        </li>
                        <li>
                            <a href="">محصول</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6"></div>
            </div>
            <br>
            <div class="card">
                <div class="container-fliud">
                    <div class="wrapper row">
                        <div class="col-md-1">
                            <div class="preview-thumbnail nav nav-tabs">
                                <a class="active" data-target="#pic-1" data-toggle="tab"><img src="image/5bdd8e31ba395.jpg" /></a>
                                <br>
                                <a data-target="#pic-2" data-toggle="tab"><img src="image/5bdd8e31ba395.jpg" /></a>
                                <br>
                                <a data-target="#pic-3" data-toggle="tab"><img src="image/5bdabcaadd415.jpg" /></a>
                                <br>
                                <a data-target="#pic-4" data-toggle="tab"><img src="image/5bdd8f3abfa92.jpg" /></a>
                            </div>
                        </div>
                        <div class="preview col-md-4 col-xs-12">
                            <div class="preview-pic tab-content">
                                <div class="tab-pane active" id="pic-1">
                                    <img class="pop" data-toggle="modal" href="#ignismyModal-0" src="image/5bdd8e31ba395.jpg" />
                                </div>
                                <div class="tab-pane" id="pic-2">
                                    <img class="pop" data-toggle="modal" href="#ignismyModal-1" src="image/5bdd8e31ba395.jpg" />
                                </div>
                                <div class="tab-pane" id="pic-3">
                                    <img class="pop" data-toggle="modal" href="#ignismyModal-2" src="image/5bdabcaadd415.jpg" />
                                </div>
                                <div class="tab-pane" id="pic-4">
                                    <img class="pop" data-toggle="modal" href="#ignismyModal-3" src="image/5bdd8f3abfa92.jpg" />
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="modal fade" id="ignismyModal-0" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="thank-you-pop">
                                                        <img src="image/5bdd8e31ba395.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="modal fade" id="ignismyModal-1" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                                                    </div>
                                                <div class="modal-body">
                                                    <div class="thank-you-pop">
                                                        <img src="image/5bdd8e31ba395.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="modal fade" id="ignismyModal-2" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                                                    </div>
                                                <div class="modal-body">
                                                    <div class="thank-you-pop">
                                                        <img src="image/5bdabcaadd415.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="modal fade" id="ignismyModal-3" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label=""><span>×</span></button>
                                                    </div>
                                                <div class="modal-body">
                                                    <div class="thank-you-pop">
                                                        <img src="image/5bdd8f3abfa92.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details col-md-7">
                            <h5 class="product-title">
                                جاروبرقی بوش مدل BSGL5PRO5
                                <br>
                                <span>Vacuum cleaner Bosch BSGL5PRO5</span>
                            </h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <br>
                                    <h4 class="price">
                                        قیمت فعلی : 
                                        <span>۱/۰۸۰/۰۰۰تومان</span>
                                    </h4>                                    
                                    <br>
                                    <!-- <h5 class="sizes">سایز:
                                    <span class="size" data-toggle="tooltip" title="small">s</span>
                                    <span class="size" data-toggle="tooltip" title="medium">m</span>
                                    <span class="size" data-toggle="tooltip" title="large">l</span>
                                    <span class="size" data-toggle="tooltip" title="xtra large">xl</span>
                                    </h5> -->
                                    <!-- <p class="product-description">Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p> -->
                                    <!-- <p class="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p> -->
                                </div>
                                <div class="col-md-6">
                                    <div class="product-list">
                                        <div class="card-body">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link" data-toggle="collapse"  href="#menu4" aria-expanded="false" aria-controls="menu3">
                                                        ویژگی های محصول
                                                    </a>
                                                </div>
                                                <div id="menu4" class="collapse">
                                                    <div class="card-body">
                                                        <p>لوله تلسکوپی: دارد </p>
                                                        <p>فیلتر بهداشتی: دارد</p>
                                                        <p>نوع فیلتر خروجی: HEPA </p>
                                                        <p>کنترل روی دسته: ندارد</p>
                                                        <p>محدوده میزان صدا: 70-75 دسی بل </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row shoping">
                                <div class="col-md-6">
                                    <h5 class="" style="color:#888585">انتخاب رنگ:
                                        <a href="">
                                            <span class="color black not-available" data-toggle="tooltip" title="Not In store"></span>
                                        </a>
                                        <a href="">
                                            <span class="color green"></span>
                                        </a>    
                                        <a href="">
                                            <span class="color blue"></span>
                                        </a>
                                    </h5>
                                </div>
                                <div class="col-md-3">
                                    <h6 class="Warranty">
                                        <img src="image/Warranty.png" alt="">
                                        <span style="color:#888585">  گارانتی ۲۴ ماهه</span>
                                    </h6>
                                </div>
                                <div class="col-md-3">
                                    <div class="rating">
                                        <div class="row">
                                            <div class="rating">
                                                <input type="radio" id="star6" name="rating" value="6" /><label for="star6" title="Meh">1 star</label>
                                                <input type="radio" id="star7" name="rating" value="7" /><label for="star7" title="Pretty good">2 stars</label>
                                                <input type="radio" id="star8" name="rating" value="8" /><label for="star8" title="Pretty good">3 stars</label>
                                                <input type="radio" id="star9" name="rating" value="9" /><label for="star9" title="Rocks!">4 stars</label>
                                                <input type="radio" id="star10" name="rating" value="10" /><label for="star10" title="Rocks!">5 stars</label>
                                            </div>
                                            <!-- <span class="review-no" style="color:#888585">   41 مشاهده</span> -->
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row shoping-2">
                                <div class="col-md-5">
                                    <div class="qty mt-5">
                                        <!-- <span class="minus bg-dark">-</span> -->
                                        <input type="number" class="count" name="qty" value="1">
                                        <!-- <span class="plus bg-dark">+</span> -->
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="share">
                                        <ul>
                                            <li><button class="add-to-cart btn btn-default" type="button"><img src="image/share.png" alt=""></button></li>
                                            <li><button class="add-to-cart btn btn-default" type="button"><span class="flaticon flaticon-like"></span></button></li>
                                            <!-- <li><button class="add-to-cart btn btn-default" type="button"></button></li> -->
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="action">
                                        <button class="add-to-cart btn btn-default" type="button">
                                            اضافه به لیست خرید
                                            <span class="flaticon flaticon-credit-card"></span>
                                        </button>
                                        <!-- <button class="like btn btn-default" type="button"><span class="fa fa-heart"></span></button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="tab-1">
                <nav>
                    <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">مشخصات</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">تیزر</a>
                        <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">نظرات کاربران</a>
                    </div>
                </nav>
                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="container">
                            <div class="row">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th><h4>مشخصات کلی</h4></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="title">ابعاد</td>
                                            <td> 240 × 307 × 465 میلی‌متر </td>
                                        </tr>
                                        <tr>
                                            <td class="title">وزن</td>
                                            <td> 6.8 کیلوگرم </td>
                                        </tr>
                                        <tr>
                                            <td class="title">نوع جارو برقی</td>
                                            <td> کیسه دار </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="container">
                            <div class="row">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th><h4>مشخصات فنی</h4></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="title">قدرت موتور</td>
                                            <td>700 وات</td>
                                        </tr>
                                        <tr>
                                            <td class="title">قابلیت جذب مایعات</td>
                                            <td>خیر</td>
                                        </tr>
                                        <tr>
                                            <td class="title">سیم جمع کن خودکار</td>
                                            <td>دارد</td>
                                        </tr>
                                        <tr>
                                            <td class="title">کنترل روی دسته</td>
                                            <td> ندارد </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <video id="blah" controls="">
                            <source src="videos/video.mp4">
                        </video>
                    </div>
                    <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        <div class="container">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                            <p class="text-secondary text-center">۱۵ روز پیش</p>
                                        </div>
                                        <div class="col-md-10">
                                            <p>
                                                <a class="float-left" href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>ناشناس</strong></a>
                                                <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                                <span class="float-right"><i class="text-warning fa fa-star"></i></span>
                                            </p>
                                            <div class="clearfix"></div>
                                            <br>
                                            <p>                                                
                                                توان این جارو چرا اینقدر پایینه باتوجه به قیمت و موارد مشابه از برند بوش و سایر برندها؟؟
                                                700 واته!!!
                                                این قدرت موتور بنده رو در خرید این محصول مردد کرده و مورد دیگه کیسه یکبار مصرف هست...اگر کیسه این جارو گیرنیاد عملا بی مصرفه دیگه؟؟!!
                                            </p>
                                            <p>
                                                <a class="float-right btn btn-outline-primary ml-2"> <i class="fa fa-reply"></i>پاسخ</a>
                                                <a class="float-right btn btn-danger"> <i class="fa fa-heart"></i>پسندیدم</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="card card-inner">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <img src="https://image.ibb.co/jw55Ex/def_face.jpg" class="img img-rounded img-fluid"/>
                                                    <p class="text-secondary text-center">۱۴ روز پیش</p>
                                                </div>
                                                <div class="col-md-10">
                                                    <p><a href="https://maniruzzaman-akash.blogspot.com/p/contact.html"><strong>ادمین</strong></a></p>
                                                    <p>
                                                        کیسه جارو برقی همیشه موجود هست فقط قیمت اصل اون یکم زیاد مدل های کیسه ترک و چینی و ایرانی هست مه به حارو برقی بخوره هر کیسه هم ظریفت استفاده حدود یک ماه شاید هم بیشتر باشه
                                                    </p>
                                                    <p>
                                                        <a href="">آیا این پاسخ برایتان مفید بود ؟ </a>
                                                        <a class="float-right btn btn-outline-primary ml-2"><i class="fa fa-reply"></i>پاسخ</a>
                                                        <a class="float-right btn btn-danger"><i class="fa fa-heart"></i>پسندیدم</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include("blocks/footer-2.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>