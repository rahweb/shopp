<!doctype html>
<html lang="en">
    <?php include("blocks/head.php");?>
  <body>
    <?php include("blocks/menu.php");?>
    <div class="product-list">
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="list-shop">
                        <h6>دسته بندی محصولات</h6>
                        <br>
                        <div class="container">
                            <div id="accordion">
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse"  href="#menuone" aria-expanded="true" aria-controls="menuone">خانه و آشپزخانه</a>
                                    </div>
                                    <div id="menuone" class="collapse show">
                                        <div class="card-body">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link" data-toggle="collapse"  href="#menu4" aria-expanded="false" aria-controls="menu3">صوتی و تصویری</a>
                                                </div>
                                                <div id="menu4" class="collapse">
                                                    <div class="card-body">
                                                        <a href=""><p>تلوزیون</p></a>
                                                        <a href=""><p>میز تلوزیون</p></a>
                                                        <a href=""><p>کنسول بازی</p></a>
                                                        <a href=""><p>سینمای خانگی و ساندبار</p></a>
                                                        <a href=""><p>سیستم صوتی خانگی</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse"  href="#menutwo" aria-expanded="false" aria-controls="menutwo">مد و پوشاک</a>
                                    </div>
                                    <div id="menutwo" class="collapse">
                                        <div class="card-body">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link" data-toggle="collapse"  href="#menu5" aria-expanded="false" aria-controls="menu3">پوشاک ورزشی</a>
                                                </div>
                                                <div id="menu5" class="collapse">
                                                    <div class="card-body">
                                                        <a href=""><p>پوشاک ورزشی آقایان</p></a>
                                                        <a href=""><p>پوشاک ورزشی خانم‌ها</p></a>
                                                        <a href=""><p>اکسسوری ورزشی</p></a>
                                                        <a href=""><p>پوشاک بانوان</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <a class="card-link" data-toggle="collapse"  href="#menu3" aria-expanded="false" aria-controls="menu3">فرهنگ و هنر</a>
                                    </div>
                                    <div id="menu3" class="collapse">
                                        <div class="card-body">
                                            <div class="card">
                                                <div class="card-header">
                                                    <a class="card-link" data-toggle="collapse"  href="#menu6" aria-expanded="false" aria-controls="menu3">لوازم التحریر</a>
                                                </div>
                                                <div id="menu6" class="collapse">
                                                    <div class="card-body">
                                                        <a href=""><p>نوشت افزار</p></a>
                                                        <a href=""><p>ابزار نقاشی و رنگ آمیزی</p></a>
                                                        <a href=""><p>ابزار طراحی و مهندسی</p></a>
                                                        <a href=""><p>لوازم اداری</p></a>
                                                        <a href=""><p>دفتر و کاغذ</p></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="searchbox">
                        <h6>جستوجو در محصولات</h6>
                        <br>
                        <div id="searchForm">
                            <div class="container">
                                <div class="row">
                                    <form class="form-row">
                                        <div class="col-10">
                                            <input class="form-control" type="search" placeholder="نوشتن نام محصول">
                                        </div>
                                        <div class="col-2">
                                            <button class="btn btn-outline-success bg-success" type="submit">
                                                <img src="image/search.png" alt="">
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="buttonbox">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-10">
                                    <h6>فقط کالاهای موجود</h6>
                                </div>
                                <div class="col-md-2">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="checkbox checbox-switch switch-primary">
                                                <label>
                                                    <input type="checkbox" name="" checked="" />
                                                    <span></span>                                        
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-10">
                                    <h6>فقط کالاهای آماده ارسال </h6>
                                </div>
                                <div class="col-md-2">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="checkbox checbox-switch switch-primary">
                                                <label>
                                                    <input type="checkbox" name="" checked="" />
                                                    <span></span>                                        
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row loc">
                        <div class="col-md-6">
                            <ul>
                                <li class="home">
                                    <a href=""><span class="flaticon flaticon-home" style="color:#ec5598"></span>فروشگاه اینترنتی</a>
                                </li>
                                <li>
                                    <a href="">مد و پوشاک</a>
                                </li>
                                <li>
                                    <a href="">پوشاک بانوان</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <br>		
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <ul class="c-listing">
                                    <li>
                                        مرتب سازی براساس:
                                    </li>
                                    <li>
                                        <a href="javascript:" class="active" data-sort="1">همه محصولات</a>
                                    </li>
                                    <li>
                                        <a href="javascript:" data-sort="2">محبوب‌ترین</a>
                                    </li>
                                    <li>
                                        <a href="javascript:" data-sort="3">جدیدترین</a>
                                    </li>
                                    <li>
                                        <a href="javascript:" data-sort="4">پرفروش‌ترین‌</a>
                                    </li>
                                </ul>
                                <span class="pull-right">
                                    <!-- Tabs -->
                                    <ul class="nav panel-tabs">
                                        <li class="active menuicon"><a href="#tab1" data-toggle="tab"><img src="image/menu.png" alt=""></a></li>
                                        <li class="gridicon"><a href="#tab2" data-toggle="tab"><img src="image/grid.png" alt=""></a></li>
                                    </ul>
                                </span>
                            </div>
                            <br>
                            <br>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab1">
                                        <div class="products">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="image/5bd5a1671ef12.jpg">
                                                                    <img class="pic-2" src="image/5bdabcaadd415.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-7.jpg">
                                                                    <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-8.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                                                                    <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-5.jpg">
                                                                    <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-6.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-7.jpg">
                                                                    <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-8.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                                                                    <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-5.jpg">
                                                                    <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-6.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-7.jpg">
                                                                    <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-8.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="product">
                                                        <div class="product-grid">
                                                            <div class="product-image">
                                                                <a href="#">
                                                                    <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-1.jpg">
                                                                    <img class="pic-2" src="http://bestjquery.com/tutorial/product-grid/demo9/images/img-2.jpg">
                                                                </a>
                                                                <ul class="social">
                                                                    <li><a href="" data-tip="افزودن به لیست دلخواه"><span class="flaticon flaticon-like"></span></a></li>
                                                                    <li><a href="" data-tip="خرید"><span class="flaticon flaticon-shopping-cart"></span></a></li>
                                                                </ul>
                                                                <span class="product-new-label">تخفیف</span>
                                                                <span class="product-discount-label">20%</span>
                                                            </div>
                                                            <!-- <ul class="rating">
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                                <li class="fa fa-star"></li>
                                                            </ul> -->
                                                            <div class="product-content">
                                                                <h3 class="title"><a href="#">تی شرت ساده مردانه</a></h3>
                                                                <div class="price">۲,۴۴۰,۰۰۰ ریال
                                                                    <span>۲,۷۰۰,۰۰۰ ریال</span>
                                                                </div>
                                                                <a class="add-to-cart" href="">مشاهده محصولات</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2">
                                        <div class="horizontal">
                                            <div class="row horiz">
                                                <div class="col-md-3">
                                                   <a href=""><img src="image/5bd5a1671ef12.jpg" alt=""></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5>جاروبرقی بوش مدل BSGL5PRO5</h5>
                                                    <hr>
                                                    <div class="star">
                                                        <img src="image/star.png" alt="">  2,3
                                                    </div>
                                                    <br>
                                                    <p>1,280,000 تومان</p>
                                                    <p>گارانتی اصالت و سلامت فیزیکی کالا</p>
                                                    <div class="discount">
                                                        <span>
                                                            ٪۴۰
                                                            <img src="image/tag.png" alt="">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 left-box">
                                                    <h5 class="Property"><img src="image/menu(1).png" alt=""> ویژگی ها</h5>
                                                    <hr>
                                                    <p>فیلتر بهداشتی : دارد</p>
                                                    <p>لوله تلسکوپی : دارد</p>
                                                    <p>نوع فیلتر خروجی : HEPA</p>
                                                    <p>قابلیت جذب مایعات : خیر</p>
                                                </div>
                                            </div>
                                            <div class="row horiz">
                                                <div class="col-md-3">
                                                    <a href=""><img src="image/5bd5a1671ef12.jpg" alt=""></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5>جاروبرقی بوش مدل BSGL5PRO5</h5>
                                                    <hr>
                                                    <div class="star">
                                                        <img src="image/star.png" alt="">  2,3
                                                    </div>
                                                    <br>
                                                    <p>1,280,000 تومان</p>
                                                    <p>گارانتی اصالت و سلامت فیزیکی کالا</p>
                                                </div>
                                                <div class="col-md-3 left-box">
                                                    <h5 class="Property"><img src="image/menu(1).png" alt=""> ویژگی ها</h5>
                                                    <hr>
                                                    <p>فیلتر بهداشتی : دارد</p>
                                                    <p>لوله تلسکوپی : دارد</p>
                                                    <p>نوع فیلتر خروجی : HEPA</p>
                                                    <p>قابلیت جذب مایعات : خیر</p>
                                                </div>
                                            </div>
                                            <div class="row horiz">
                                                <div class="col-md-3">
                                                    <a href=""><img src="image/5bd5a1671ef12.jpg" alt=""></a>
                                                </div>
                                                <div class="col-md-6">
                                                    <h5>جاروبرقی بوش مدل BSGL5PRO5</h5>
                                                    <hr>
                                                    <div class="star">
                                                        <img src="image/star.png" alt="">  2,3
                                                    </div>
                                                    <br>
                                                    <p>1,280,000 تومان</p>
                                                    <p>گارانتی اصالت و سلامت فیزیکی کالا</p>
                                                </div>
                                                <div class="col-md-3 left-box">
                                                    <h5 class="Property"><img src="image/menu(1).png" alt=""> ویژگی ها</h5>
                                                    <hr>
                                                    <p>فیلتر بهداشتی : دارد</p>
                                                    <p>لوله تلسکوپی : دارد</p>
                                                    <p>نوع فیلتر خروجی : HEPA</p>
                                                    <p>قابلیت جذب مایعات : خیر</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
    <?php include("blocks/footer-2.php");?>
    <?php include("blocks/script.php");?>
  </body>
</html>